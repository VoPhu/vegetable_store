
<%@page import="com.models.Cart"%>
<%@page import="com.models.GenerateID"%>
<%@page import="com.DAO.CartDAO"%>
<%@page import="com.models.User"%>
<%@page import="com.DAO.UserDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.models.Product"%>
<%@page import="com.DAO.ProductDAO"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.DAO.CateogoryDAO"%>
<jsp:include page="header.jsp" />
<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="/img/breadcrumb.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Organi Shop</h2>
                    <div class="breadcrumb__option">
                        <a href="/">Home</a>
                        <span>Shop</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<!-- Product Section Begin -->
<section class="product spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-5">
                <div class="sidebar">
                    <div class="sidebar__item">
                        <h4>Department</h4>
                        <ul>
                            <%
                                CateogoryDAO cdao = new CateogoryDAO();
                                ResultSet set = cdao.getAllCategory();
                                while (set.next()) {
                            %>
                            <li><a href="?id=<%= set.getString("catagory_id")%>" ><%= set.getString("category_name")%></a></li>
                                <%
                                    }
                                %>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-7">



                <div class="filter__item">
                    <div class="row">
                        <div class="col-lg-4 col-md-5">
                            <div class="filter__sort">
                                <span>Sort By</span>
                                <select id="mySelect" onchange="reloadPage()">
                                    <option onclick="increasing">Increasing</option>
                                    <option onclick="decreasing">Decreasing</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <%
                                ProductDAO dAO = new ProductDAO();
                                int count_product = 0;
                            %>
                            <div class="filter__found">
                                <%
                                    if (request.getParameter("id") != null) {
                                        String category_id = request.getParameter("id");
                                        count_product = dAO.countgetAllProductByCategory(category_id);
                                %><h6><span>
                                        <%= count_product%>
                                    </span> Products found</h6><%
                                    } else {
                                        count_product = dAO.countProduct();
                                    %>   <h6><span>
                                        <%= count_product%>
                                    </span> Products found</h6><%
                                        }
                                    %>


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-3">
                            <div class="filter__option">
                                <span class="icon_grid-2x2"></span>
                                <span class="icon_ul"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">


                    <%
                        if (request.getParameter("order") != null) {
                            String email = session.getAttribute("name").toString();
                            UserDAO userDAO = new UserDAO();
                            User user = userDAO.getUserByEmail(email);
                            String product_id = request.getParameter("order");
                            CartDAO cartDAO = new CartDAO();
                            int check = 0;
                            if (cartDAO.checkIDCartInProduct(product_id)) {
                                GenerateID generateID = new GenerateID();
                                Cart cart = new Cart(generateID.generateCart(), 1, product_id, user.getUser_id());
                                ProductDAO productDAO = new ProductDAO();
                                check = productDAO.addToCardProduct(cart);
                            } else {
                                Cart cart = cartDAO.getCartByProduct(product_id);
                                ProductDAO productDAO = new ProductDAO();
                                check = productDAO.addToCardProduct(cart.getQuantity() + 1, product_id);
                            }

                            if (check != 0) {
                    %>
                    <script>
                        window.alert("Add to cart success");
                    </script>
                    <%
                    } else {
                    %>
                    <script>
                        window.alert("Add to cart fail");
                    </script>
                    <%
                            }
                        }
                        if (request.getParameter("id") != null) {
                            String category_id = request.getParameter("id");
                            ProductDAO pdao = new ProductDAO();
                            ResultSet rs = pdao.getAllProductByCategory(category_id);
                            while (rs.next()) {
                    %>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="product__item">
                            <div class="product__item__pic set-bg" data-setbg="/img/product/<%= rs.getString("image")%>">
                                <ul class="product__item__pic__hover">
                                    <%
                                        if (session.getAttribute("login_done") != null) {
                                    %>
                                    <li><a href="<%= request.getContextPath()%>?order=<%= rs.getString("product_id")%>"><i class="fa fa-shopping-cart"></i></a></li>
                                            <%
                                            } else {
                                            %>
                                    <li><a href="/account/login"><i class="fa fa-shopping-cart"></i></a></li>
                                            <%
                                                }
                                            %>
                                </ul>
                            </div>
                            <div class="product__item__text">
                                <h6><a href="#"><%= rs.getString("product_name")%></a></h6>
                                <h5>$<%= rs.getString("selling_price")%></h5>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                    } else if (request.getParameter("option") != null) {
                        ProductDAO productDAO = new ProductDAO();
                        if (request.getParameter("option").equals("Increasing")) {

                            ResultSet set11 = productDAO.getAllProductIncreasing();
                            while (set11.next()) {
                    %>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="product__item">
                            <div class="product__item__pic set-bg" data-setbg="/img/product/<%= set11.getString("image")%>">
                                <ul class="product__item__pic__hover">

                                    <%
                                        if (session.getAttribute("login_done") != null) {
                                    %>
                                    <li><a href="<%= request.getContextPath()%>?order=<%= set11.getString("product_id")%>"><i class="fa fa-shopping-cart"></i></a></li>
                                            <%
                                            } else {
                                            %>
                                    <li><a href="/account/login"><i class="fa fa-shopping-cart"></i></a></li>
                                            <%
                                                }
                                            %>
                                </ul>
                            </div>
                            <div class="product__item__text">
                                <h6><a href="<%= request.getContextPath()%>/product/details?id=<%= set11.getString("product_id")%>"><%= set11.getString("product_name")%></a></h6>
                                <h5>$<%= set11.getString("selling_price")%></h5>
                            </div>
                        </div>
                    </div>
                    <%
                        }

                    } else {
                        ResultSet set12 = productDAO.getAllProductDecreasing();
                        while (set12.next()) {
                    %>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="product__item">
                            <div class="product__item__pic set-bg" data-setbg="/img/product/<%= set12.getString("image")%>">
                                <ul class="product__item__pic__hover">

                                    <%
                                        if (session.getAttribute("login_done") != null) {
                                    %>
                                    <li><a href="<%= request.getContextPath()%>?order=<%= set12.getString("product_id")%>"><i class="fa fa-shopping-cart"></i></a></li>
                                            <%
                                            } else {
                                            %>
                                    <li><a href="/account/login"><i class="fa fa-shopping-cart"></i></a></li>
                                            <%
                                                }
                                            %>
                                </ul>
                            </div>
                            <div class="product__item__text">
                                <h6><a href="<%= request.getContextPath()%>/product/details?id=<%= set12.getString("product_id")%>"><%= set12.getString("product_name")%></a></h6>
                                <h5>$<%= set12.getString("selling_price")%></h5>
                            </div>
                        </div>
                    </div>


                    <%
                            }

                        }
                    } else {

                        ProductDAO pdao = new ProductDAO();
                        ResultSet rs = pdao.getAllProduct();
                        while (rs.next()) {
                    %>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="product__item">
                            <div class="product__item__pic set-bg" data-setbg="/img/product/<%= rs.getString("image")%>">
                                <ul class="product__item__pic__hover">
                                    <%
                                        if (session.getAttribute("login_done") != null) {
                                    %>
                                    <li><a href="<%= request.getContextPath()%>?order=<%= rs.getString("product_id")%>"><i class="fa fa-shopping-cart"></i></a></li>
                                            <%
                                            } else {
                                            %>
                                    <li><a href="/account/login"><i class="fa fa-shopping-cart"></i></a></li>
                                            <%
                                                }
                                            %>
                                </ul>
                            </div>
                            <div class="product__item__text">
                                <h6><a href="#"><%= rs.getString("product_name")%></a></h6>
                                <h5>$<%= rs.getString("selling_price")%></h5>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>
                <%
                    }

                %>


            </div>

        </div>

    </div>
</section>
<!-- Product Section End -->
<script>
    function reloadPage() {
        var selectedOption = document.getElementById("mySelect").value;
        window.location.href =
                window.location.href.split("?")[0] + "?option=" + selectedOption;
    }
</script>


<jsp:include page="footer.jsp" />