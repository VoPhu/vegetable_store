
<%@page import="com.models.Cart"%>
<%@page import="com.models.GenerateID"%>
<%@page import="com.DAO.CartDAO"%>
<%@page import="com.models.User"%>
<%@page import="com.DAO.UserDAO"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.DAO.ProductDAO"%>
<%@page import="com.models.Product"%>
<jsp:include page="header.jsp" />
<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="/img/breadcrumb.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Vegetable Package</h2>
                    <div class="breadcrumb__option">
                        <a href="/">Home</a>
                        <a href="/">Vegetables</a>
                        <span>Vegetable Package</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<%
    if (request.getParameter("id") == null) {
        response.sendRedirect("/");
    }

    String id = request.getParameter("id");
    ProductDAO pdao = new ProductDAO();
    Product product = pdao.getAllByID(id);

%>

<%          if (request.getParameter("id1") != null) {
        String email = session.getAttribute("name").toString();
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUserByEmail(email);
        String product_id = request.getParameter("id1");
        CartDAO cartDAO = new CartDAO();
        int check = 0;
        if (cartDAO.checkIDCartInProduct(product_id)) {
            GenerateID generateID = new GenerateID();
            Cart cart = new Cart(generateID.generateCart(), 1, product_id, user.getUser_id());
            ProductDAO productDAO = new ProductDAO();
            check = productDAO.addToCardProduct(cart);
        } else {
            Cart cart = cartDAO.getCartByProduct(product_id);
            ProductDAO productDAO = new ProductDAO();
            check = productDAO.addToCardProduct(cart.getQuantity() + 1, product_id);
        }

        if (check != 0) {
%>
<script>
    window.alert("Add to cart success");
</script>
<%
} else {
%>
<script>
    window.alert("Add to cart fail");
</script>
<%
        }
    }

%>


<%          if (request.getParameter("btn_add_to_card") != null) {
        String email = session.getAttribute("name").toString();
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUserByEmail(email);
        String product_id = request.getParameter("id");
        int quantity = Integer.valueOf(request.getParameter("quantity"));
        CartDAO cartDAO = new CartDAO();
        int check = 0;
        if (cartDAO.checkIDCartInProduct(product_id)) {
            GenerateID generateID = new GenerateID();
            Cart cart = new Cart(generateID.generateCart(), quantity, product_id, user.getUser_id());
            ProductDAO productDAO = new ProductDAO();
            check = productDAO.addToCardProduct(cart);
        } else {
            Cart cart = cartDAO.getCartByProduct(product_id);
            ProductDAO productDAO = new ProductDAO();
            check = productDAO.addToCardProduct(cart.getQuantity() + 1, product_id);
        }

        if (check != 0) {
%>
<script>
    window.alert("Add to cart success");
</script>
<%
} else {
%>
<script>
    window.alert("Add to cart fail");
</script>
<%
        }
    }

%>



<!-- Product Details Section Begin -->
<section class="product-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product__details__pic">
                    <div class="product__details__pic__item">
                        <img class="product__details__pic__item--large"
                             src="/img/product/<%= product.getImage()%>" alt="<%= product.getProduct_name()%>">
                    </div>
                    <div class="product__details__pic__slider owl-carousel">
                        <img data-imgbigurl="/img/product/details/product-details-2.jpg"
                             src="/img/product/details/thumb-1.jpg" alt="">
                        <img data-imgbigurl="/img/product/details/product-details-3.jpg"
                             src="/img/product/details/thumb-2.jpg" alt="">
                        <img data-imgbigurl="/img/product/details/product-details-5.jpg"
                             src="/img/product/details/thumb-3.jpg" alt="">
                        <img data-imgbigurl="/img/product/details/product-details-4.jpg"
                             src="/img/product/details/thumb-4.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product__details__text">
                    <h3>Vetgetable Package</h3>
                    <div class="product__details__rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>

                    </div>
                    <div class="product__details__price">$<%= product.getSelling_price()%></div>
                    <p>Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam
                        vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Vestibulum ac diam sit amet
                        quam vehicula elementum sed sit amet dui. Proin eget tortor risus.</p>
                        <%
                            if (session.getAttribute("login_done") != null) {
                        %>
                    <form action="" method="get">
                        <div class="product__details__quantity">
                            <input type="hidden" name="id" value="<%= product.getProduct_id() %>">
                            <div class="quantity">
                                <div class="pro-qty">
                                    <input type="text" name="quantity" value="1">
                                </div>
                            </div>
                        </div>
                        <input type="submit" name="btn_add_to_card" class="primary-btn" value="ADD TO CARD">
                    </form>
                    <%
                    } else {
                    %>
                    <div class="product__details__quantity">
                        <div class="quantity">
                            <div class="pro-qty">
                                <input type="text" name="" value="1">
                            </div>
                        </div>
                    </div>
                    <a href="<%= request.getContextPath()%>/account/login" class="primary-btn">ADD TO CARD</a>
                    <%
                        }
                    %>

                    <ul>

                        <!--  -->
                        <li><b>Availability</b> <span>In Stock</span></li>
                        <li><b>Shipping</b> <span>01 day shipping. <samp>Free pickup today</samp></span></li>
                        <li><b>Weight</b> <span>0.5 kg</span></li>
                        <li><b>Share on</b>
                            <div class="share">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="product__details__tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                               aria-selected="true">Description</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Products Infomation</h6>
                                <p><%= product.getProduct_desc()%></p>
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                    ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                    elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                    porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                    nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.
                                    Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed
                                    porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum
                                    sed sit amet dui. Proin eget tortor risus.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product Details Section End -->

<!-- Related Product Section Begin -->
<section class="related-product">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title related__product__title">
                    <h2>Related Product</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <%
                ResultSet rs = pdao.getAllProductByCategory(product.getCategory_id());
                while (rs.next()) {
            %>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="product__item">
                    <div class="product__item__pic set-bg" data-setbg="/img/product/<%= rs.getString("image")%>">
                        <ul class="product__item__pic__hover">
                            <%
                                if (session.getAttribute("login_done") != null) {
                            %>
                            <li><a href="<%= request.getContextPath()%>?id1=<%= rs.getString("product_id")%>"><i class="fa fa-shopping-cart"></i></a></li>
                                    <%
                                    } else {
                                    %>
                            <li><a href="/account/login"><i class="fa fa-shopping-cart"></i></a></li>
                                    <%
                                        }
                                    %>
                        </ul>
                    </div>
                    <div class="product__item__text">
                        <h6><a href="#"><%= rs.getString("product_name")%></a></h6>
                        <h5>$<%= rs.getString("selling_price")%></h5>
                    </div>
                </div>
            </div>
            <%
                }
            %>

        </div>
    </div>
</section>
<!-- Related Product Section End -->

<jsp:include page="footer.jsp" />